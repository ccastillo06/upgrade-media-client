import React from 'react';

import './styles.css';

function Profile(props) {
  const { person } = props;

  return (
    <>
        <h4>The person profile:</h4>
        <ul>
          {Object.keys(person).map(key => {
            return <li key={key}>{key}: {person[key]}</li>
          })}
        </ul>
      </>
  );
}

// class Profile extends React.Component {
//   render() {
//     const { person } = this.props;

//     return (
//       <>
//         <h4>The person profile:</h4>
//         <ul>
//           {Object.keys(person).map(key => {
//             return <li key={key}>{key}: {person[key]}</li>
//           })}
//         </ul>
//       </>
//     )
//   }
// }

export default Profile;
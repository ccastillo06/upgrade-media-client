import React from 'react';

import './styles.css';

// function Title(props) {
//   return <h1>Upgrade media</h1>;
// }

class Title extends React.Component {
  render() {
    const { title, tech } = this.props;

    return (
      <>
        <h1>The title is: {title}</h1>
        <h3>Today we are learning {tech}</h3>
      </>
    );
  }
}

export default Title;
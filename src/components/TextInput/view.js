import React, { Component, useState } from 'react';

function TextInput() {
  const [inputValue, setInputValue] = useState('Hello World');

  function handleOnChange(e) {
    const { value } = e.target;
    setInputValue(value);
  }

  return (
    <>
      <h4>Example input text</h4>
      <input type="text" value={inputValue} onChange={handleOnChange} />
      <h5>{inputValue}</h5>
    </>
  )
}

// class TextInput extends Component {
//   state = {
//     value: 'hello world',
//   };
  

//   handleOnChange = (e) => {
//     const { value } = e.target;
//     this.setState({ value })
//   } 

//   render() {
//     return (
//       <>
//       <h4>Example input text</h4>
//       <input type="text" value={this.state.value} onChange={this.handleOnChange} />
//       <h5>{this.state.value}</h5>
//       </>
//     );
//   }
// }

export default TextInput;
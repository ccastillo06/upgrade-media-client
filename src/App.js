import React from 'react';

import Title from './components/Title';
import Profile from './components/Profile';
import TextInput from './components/TextInput';

import './App.css';

// function App() {
//   return (
//     <div className="App">
//      <Title />
//     </div>
//   );
// }

class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.person = {
      name: 'pepe',
      surname: 'gotera',
      age: 30,
      job: 'developer',
      city: 'Madrid'
    }
  }

  render() {
    return (
      <div className="App">
        <Title title="Upgrade Media" tech="React" />
        <Profile person={this.person} />
        <TextInput />
      </div>
    );
  }
}

export default App;